package vandy.mooc.model;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

import vandy.mooc.MVP;
import vandy.mooc.common.Utils;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

/**
 * This class plays the "Model" role in the Model-View-Presenter (MVP)
 * pattern by defining an interface for providing data that will be
 * acted upon by the "Presenter" and "View" layers in the MVP pattern.
 * It implements the MVP.ProvidedModelOps so it can be created/managed
 * by the GenericPresenter framework.
 */
public class ImageDownloadsModel 
       implements MVP.ProvidedModelOps {
    /**
     * Debugging tag used by the Android logger.
     */
    protected final static String TAG = 
        ImageDownloadsModel.class.getSimpleName();

    /**
     * A WeakReference used to access methods in the Presenter layer.
     * The WeakReference enables garbage collection.
     */
    private WeakReference<MVP.RequiredPresenterOps> mPresenter;

    /**
     * Hook method called when a new instance of AcronymModel is
     * created.  One time initialization code goes here, e.g., storing
     * a WeakReference to the Presenter and initializing the sync and
     * async Services.
     * 
     * @param presenter
     *            A reference to the Presenter layer.
     */
    @Override
    public void onCreate(MVP.RequiredPresenterOps presenter) {
        // Set the WeakReference.
        mPresenter =
            new WeakReference<>(presenter);
    }

    /**
     * Hook method called to shutdown the Model layer.
     *
     * @param isChangingConfigurations
     *        True if a runtime configuration triggered the onDestroy() call.
     */
    @Override
    public void onDestroy(boolean isChangingConfigurations) {
        // No-op.
    }

    /**
     * Download the image located at the provided Internet url using
     * the URL class, store it on the android file system using a
     * FileOutputStream, and return the path to the image file on
     * disk.
     *
     * @param context
     *          The context in which to write the file.
     * @param url 
     *          The URL of the image to download.
     * @param directoryPathname 
     *          Pathname of the directory to write the file.
     * 
     * @return 
     *        Absolute path to the downloaded image file on the file
     *        system.
     */
    public Uri downloadImage(Context context,
                             Uri url,
                             Uri directoryPathname) {
        // @@ TODO -- You fill in here, replacing "null" with the appropriate code.
        
        String lastPathSegment = url.getLastPathSegment();
        // sanity check 
        if (lastPathSegment == null) {
            Log.d(TAG, "downloadImage, invalid url: " + url);
            return null;
        }
        
        URL imageURL =  null;
        try {
            imageURL = new URL(url.toString());
        } catch (MalformedURLException e) {
            Log.d(TAG, e.getMessage(), e);
            return null;
        }
        
        Uri tmpImageDir;
        Uri tmpImageFile;
        try {
            File tmpFile = File.createTempFile(lastPathSegment, null);
            tmpFile.deleteOnExit();
            tmpImageFile = Uri.parse(tmpFile.getName());
            tmpImageDir = Uri.parse(tmpFile.getParent());
        } catch (IOException e) {
            Log.d(TAG, e.getMessage(), e);
            return null;
        }
        
        return Utils.createDirectoryAndSaveFile(context, imageURL, tmpImageFile, tmpImageDir);
    }
    
    
}

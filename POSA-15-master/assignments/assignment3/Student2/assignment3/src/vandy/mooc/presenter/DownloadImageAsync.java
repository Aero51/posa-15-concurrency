package vandy.mooc.presenter;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.lang.ref.WeakReference;

import vandy.mooc.MVP;
import vandy.mooc.common.Utils;
import vandy.mooc.model.ImageDownloadsModel;


public class DownloadImageAsync extends AsyncTask<Uri,Void,Uri> {

    private static final String TAG = DownloadImageAsync.class.getName();

    private Uri path;

    private ImagePresenter imagepresenter;

    public DownloadImageAsync(Uri path, ImagePresenter imagepresenter) {
        this.path = path;
        this.imagepresenter = imagepresenter;
    }

    @Override
    protected Uri doInBackground(Uri... uris) {
        ImageDownloadsModel im = new ImageDownloadsModel();
        Uri uribase = uris[0];
        Uri imagePath = im.downloadImage(imagepresenter.getApplicationContext(), uribase, path);
        return imagePath;
    }

    @Override
    protected void onPostExecute(Uri imagePath) {   
          imagepresenter.onProcessingComplete(path,imagePath);
    }
}


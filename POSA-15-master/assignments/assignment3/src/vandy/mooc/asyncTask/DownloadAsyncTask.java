package vandy.mooc.asyncTask;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import vandy.mooc.common.GenericAsyncTask;
import vandy.mooc.common.GenericAsyncTaskOps;
import vandy.mooc.presenter.ImagePresenter;

public class DownloadAsyncTask extends AsyncTask<Uri, Integer, Uri>{

	

private	ImagePresenter presenter;
	private Uri mDirectoryPathname;
	
	
	public DownloadAsyncTask(ImagePresenter presenter) {
		//super(imagePresenter);
		// TODO Auto-generated constructor stub
		
		this.presenter=presenter;
		
	}


	@Override
	protected Uri doInBackground(Uri... params) {
		
		mDirectoryPathname=params[1];
		Log.d("Test before","params[0]: "+ params[0]+" , mDirectoryPathname: "+mDirectoryPathname);
		 Uri uriLocalPath=	presenter.getModel().downloadImage(presenter.getActivityContext(), params[0],mDirectoryPathname);
		Log.d("Test after", uriLocalPath.toString()+" THEEND");
		
		
		
		// TODO Auto-generated method stub
		//return super.doInBackground(params);
		return uriLocalPath;
	}
	

	@Override
	protected void onPostExecute(Uri result) {
		// TODO Auto-generated method stub
		//super.onPostExecute(result);
		presenter.onProcessingComplete(result, mDirectoryPathname);
	}

	
	
	
	
}

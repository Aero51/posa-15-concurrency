package vandy.mooc.presenter;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import vandy.mooc.MVP;
import vandy.mooc.common.BitmapUtils;
import vandy.mooc.common.DownloadUtils;
import vandy.mooc.common.GenericPresenter;
import vandy.mooc.common.Utils;
import vandy.mooc.model.ImageDownloadsModel;




/**
 * This class filters the bitmap image in the background
 *        using AsyncTask.
 */
public class FilterAsyncTask 
       extends AsyncTask<Uri, Integer, Uri> {
    /**
     * WeakReference enable garbage collection of Activity.
     */
  
    private Uri mDirectoryPathname = null;
    
    private WeakReference<MVP.RequiredPresenterOps> mPresenter;
    private WeakReference<MVP.RequiredViewOps> mView;
    /**
     * A CountDownLatch that ensures all Threads exit as a group.
     */
    private CountDownLatch mExitBarrier;
    private int mIndex;

   
    /**
     * Constructor initializes the field.
     */
    FilterAsyncTask(MVP.RequiredPresenterOps presenter, Uri mDirPath, CountDownLatch exitBarrier, int index) {
        mPresenter =
                new WeakReference<>(presenter);
        mDirectoryPathname = mDirPath;
        mExitBarrier = exitBarrier;
        mIndex = index;
    }

    /**
     * Called by the AsyncTask framework in the UI Thread to perform
     * initialization actions.
     */
    protected void onPreExecute() {
        // Show the progress dialog before starting the download
        // in a Background Thread.
        // mActivity.get().showDialog("downloading via AsyncTask");
    }

    /**
     * Apply grayscale filter on bitmap in an AsyncTask background thread.
     * 
     * @param params
     *            The url of a bitmap image
     */
    protected Uri doInBackground(Uri... urls) {
         
    	Uri mUri;
    	
    	// Block the thread till the image is available 
    	mPresenter.get().lockAcquire(mIndex);
    	
    	mUri = mPresenter.get().getimageResult(mIndex);
        
        // return BitmapUtils.grayScaleFilter(mPresenter.get().getActivityContext(), urls[0], mDirectoryPathname);  
    	return BitmapUtils.grayScaleFilter(mPresenter.get().getActivityContext(), mUri, mDirectoryPathname); 
        
    }

    /**
     * Called after an operation executing in the background is
     * completed. It sets the bitmap image to an image view 
     * 
     * @param uri
     *            The filtered bitmap image uri
     */
    protected void onPostExecute(Uri mUri) {
       
        // Display the downloaded and filtered image to the user.
       
    	mExitBarrier.countDown();
    	mPresenter.get().lockRelease(mIndex);
    	
    	// Delete intermediate file
    	File file = new File(mPresenter.get().getimageResult(mIndex).getPath());
    	file.delete();
    	
    	mPresenter.get().onProcessingComplete(mUri, mDirectoryPathname);
    }
}


package vandy.mooc.presenter;

import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import vandy.mooc.MVP;
import vandy.mooc.common.DownloadUtils;
import vandy.mooc.common.GenericPresenter;
import vandy.mooc.common.Utils;
import vandy.mooc.model.ImageDownloadsModel;




/**
 * This class downloads a bitmap image in the background
 *        using AsyncTask.
 */
public class DownloadWithAsyncTask 
       extends AsyncTask<Uri, Integer, Uri> {
    /**
     * WeakReference enable garbage collection of Activity.
     */
  
    private Uri mDirectoryPathname = null;
    
    private WeakReference<MVP.RequiredPresenterOps> mPresenter;
    private WeakReference<MVP.RequiredViewOps> mView;
    /**
     * A CountDownLatch that ensures all Threads exit as a group.
     */
    private CountDownLatch mExitBarrier;
    private int mIndex;

   
    /**
     * Constructor initializes the field.
     */
    DownloadWithAsyncTask(MVP.RequiredPresenterOps presenter, Uri mDirPath, CountDownLatch exitBarrier, int index ) {
        mPresenter =
                new WeakReference<>(presenter);
        mDirectoryPathname = mDirPath;
        mExitBarrier = exitBarrier;
        mIndex = index;
    }

    /**
     * Called by the AsyncTask framework in the UI Thread to perform
     * initialization actions.
     */
    protected void onPreExecute() {
        // Show the progress dialog before starting the download
        // in a Background Thread.
        // mActivity.get().showDialog("downloading via AsyncTask");
    }

    /**
     * Downloads bitmap in an AsyncTask background thread.
     * 
     * @param params
     *            The url of a bitmap image
     */
    protected Uri doInBackground(Uri... urls) {
         
        // return ImageDownloadsModel.downloadImage(Context, urls[0], mDirectoryPathname);
        return DownloadUtils.downloadImage(mPresenter.get().getActivityContext(), urls[0], mDirectoryPathname);

    }

    /**
     * Called after an operation executing in the background is
     * completed. It sets the bitmap image to an image view and
     * dismisses the progress dialog.
     * 
     * @param image
     *            The bitmap image uri
     */
    protected void onPostExecute(Uri mUri) {
        
        // Display the downloaded image to the user.
       
    	mExitBarrier.countDown();
    	mPresenter.get().putimageResult(mIndex, mUri);
    	mPresenter.get().lockRelease(mIndex);
    	
    	// mPresenter.get().onProcessingComplete(mUrl, mDirectoryPathname);
    }
}

